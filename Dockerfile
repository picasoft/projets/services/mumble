FROM debian:bookworm-slim

# Install Mumble server
RUN apt-get update -y \
    && apt-get install -y \
      libssl-dev \
      libbz2-dev \
      mumble-server=1.3.4-4 \
      procps \
      python3 \
      python3-pip \
      zeroc-ice-slice \
    && rm -rf /var/lib/apt/lists/*

# Install prometheus exporter and Python dependencies
COPY requirements.txt /
RUN pip3 install --break-system-package --no-cache-dir -r requirements.txt
COPY exporter.py /

EXPOSE 64738/tcp 64738/udp 8000/tcp

# Volume for persistent storage (config file and database)
VOLUME ["/data"]

# Add entrypoint
COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
